const UserModel = require("../models/user.model");

class UserController {
  registerUser(req, res, next) {
    const user = new UserModel(req.body);
    user.save(function (err, success) {
      if (err) {
        res.json(err);
      } else {
        res.json(success);
      }
    });
  }

  getAllUsers(req, res, next) {
    UserModel.find({}, "", function (err, sucess) {
      if (err) {
        res.json(err);
      } else {
        res.json(sucess);
      }
    });
  }

  getOneUser(req, res, next) {
    User.Model.findOne({
      _id: req.params.id,
    })
      .then((user) => {
        res.json(user);
      })
      .catch((err) => {
        res.json(err);
      });
  }

  updateUser(req, res, next) {
    UserModel.updateOne(
      {
        _id: req.params.id,
      },
      {
        $set: req.body,
      },
      {
        upsert: true,
      }
    )
      .then((success) => {
        res.json(req.body);
      })
      .catch((error) => {
        res.json(error);
      });
  }

  deleteUser(req, res, next) {
    UserModel.deleteOne({
      _id: req.params.id,
    })
      .then((success) => {
        res.json(success);
      })
      .catch((error) => {
        res.json(error);
      });
  }
}
