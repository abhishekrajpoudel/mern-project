const express = require("express");
const dbConfig = require("../config/db.config");

const router = express.Router();

router
  .route("/")
  .get((req, res, next) => {
    MongoClient.connect(dbUrl, function (err, client) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        const db = client.db(dbName);
        db.collection("products")
          .find()
          .sort({
            _id: -1,
          })
          .toArray()
          .then((products) => {
            res.json(products);
          })
          .catch((err) => {
            res.json({
              error: errs,
            });
          });
      }
    });
  })
  .post((req, res, next) => {
    MongoClient.connect(dbUrl, function (err, client) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        const db = client.db(dbName);
        db.collection("products")
          .insertOne(req.body)
          .then((success) => {
            res.json(req.body);
          })
          .catch((err) => {
            res.json({
              error: errs,
            });
          });
      }
    });
  });
router
  .route("/:id")
  .put((req, res, next) => {
    MongoClient.connect(dbUrl, function (err, client) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        const db = client.db(dbName);
        db.collection("products")
          .updateOne(
            {
              _id: new ObjectId(req.params.id),
            },
            {
              $set: req.body,
            },
            {
              upsert: true,
            }
          )
          .then((success) => {
            res.json(req.body);
          })
          .catch((err) => {
            res.json({
              error: errs,
            });
          });
      }
    });
  })
  .delete((req, res, next) => {
    MongoClient.connect(dbUrl, function (err, client) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        const db = client.db(dbName);
        db.collection("products")
          .deleteOne({
            _id: new ObjectId(req.params.id),
          })
          .then((success) => {
            res.json(success);
          })
          .catch((err) => {
            res.json({
              error: errs,
            });
          });
      }
    });
  })
  .get((req, res, next) => {
    MongoClient.connect(dbUrl, function (err, client) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        const db = client.db(dbName);
        db.collection("products")
          .findOne({
            _id: new ObjectId(req.params.id),
          })
          .then((product) => {
            res.json(product);
          })
          .catch((err) => {
            res.json({
              error: errs,
            });
          });
      }
    });
  });

module.exports = router;
