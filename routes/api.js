const indexRoute = require("./index");
//const authRoute = require("./auth");
const productRoute = require("./product");
const userRoute = require("./users");
const router = require("./product");
const route = require("express").Router();

router.use("/", indexRoute);
//router.use("/auth", route);
router.use("/product", productRoute);
router.use("/user", userRoute);

module.exports = router;
