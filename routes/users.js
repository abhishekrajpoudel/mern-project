var express = require("express");
const res = require("express/lib/response");
var router = express.Router();
const controller = require("../controllers/user.controller");

const userController = new controller();

/* GET users listing. */
router
  .route("/")
  .post(userController.registerUser)
  .get(userController.getAllUsers);

router
  .route("/:id")
  .get(userController.getOneUser)
  .put(userController.updateUser)
  .delete(userController.deleteUser);

module.exports = router;
